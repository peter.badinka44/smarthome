<div id="rightSidePanel"
  :class="[ showRightSidePanel ? 'right-side-panel-out' : 'right-side-panel-in' ]"
  class="pt-4"
  style="width: 225px;
  display: none;">

  <div 
    class="pb-4 pt-2 d-flex"
    style="font-size: 20px;
    font-weight: bold; 
    color: #7cfbfd;">
    <img src="./img/btn_right_filter.svg" alt="" width="40px" height="30px" 
      style="cursor: pointer;"
      @click="showRightSidePanel = !showRightSidePanel"
    >
  </div>

  <div style="display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    width: 100%;
    padding-bottom: 100px;">
    IN PROGRESS...
  </div>  
  
  <!-- <div
    class="d-flex"
    style="font-size: 16px; 
    font-family: 'Lucida Sans Regular';
    width: 100%;"
    v-for="(product, index) in bag" 
    :key="product.id">
    {{ product.head }}
    <div class="mr-auto"></div>
    <div style="cursor: pointer; 
    padding: 0px 5px 0px 5px" 
    @click="removeFromBag(product)">
    x
    </div>
  </div>

  <div 
    class="pt-5"
    style="font-size: 12px;">
    Jméno a přijmení
    <br>
    <input 
      type="text"
      style="width: 100%;
      background-color:rgba(0, 0, 0, 0);
      border: 0px;
      color: white;
      border-bottom: 1px solid white;">
  </div>

  <div 
    class="pt-3"
    style="font-size: 12px;">
    E-mail
    <br>
    <input 
      type="text"
      style="width: 100%;
      background-color:rgba(0, 0, 0, 0); 
      border: 0px;
      color: white;
      border-bottom: 1px solid white;">
  </div>

  <div 
    class="pt-3"
    style="font-size: 12px;">
    Telefon
    <br>
    <input 
      type="text"
      style="width: 100%;
      background-color:rgba(0, 0, 0, 0); 
      border: 0px;
      color: white;
      border-bottom: 1px solid white;">
  </div>

  <div 
    class="pt-3"
    style="font-size: 12px;">
    Adresa
    <br>
    <input 
      type="text"
      style="width: 100%;
      background-color:rgba(0, 0, 0, 0); 
      border: 0px;
      color: white;
      border-bottom: 1px solid white;">
  </div>

  <button 
    class="btn-pozadat-o-consultaci my-5">
    <img src="./img/btn_check.svg" alt="" width="10px" height="10px">
    POŽÁDAT O KONZULTACI
  </button>   -->

</div>

<!-- white bag -->
<div 
  style="position: fixed;
  right: 10px;
  top: 10px;
  color: black;"
  v-if="showWhiteBag()">
  <img 
    src="./img/bag.svg" alt="" width="50px" height="50px" 
    style="cursor: pointer;" fill="blue"
    @click="toggleRightSidePanel()"
  >
</div>

<!-- blue bag + badge -->
<div     
    style="position: fixed;
    right: 10px;
    top: 10px;
    color: black;"
    @click="toggleRightSidePanel()"
    v-if="showBlueBag()">
    <img 
      src="./img/bag_badge.svg" alt="" width="50px" height="50px" 
      style="cursor: pointer;" fill="blue"      
    >
      <div
        style="position: fixed;
        right: 9.5px;
        top: 13px;
        font-weight: 1000;
        font-size: 14px;
        cursor: pointer;
        height: 26px;
        width: 26px;">
        <div class="centered">
          {{ bag.length }}
      </div>
    </div>
  </div>