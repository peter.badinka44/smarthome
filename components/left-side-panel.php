<div id="leftSidePanel"
  :class="[ showLeftSidePanel ? 'left-side-panel-out' : 'left-side-panel-in' ]"
  style="width: 225px;">

  <div 
    class="mb-3 d-flex"
    style="font-size: 20px;
    font-weight: 1000;
    color: #33d4ff;
    cursor: pointer;"
    @click="showLeftSidePanel = !showLeftSidePanel">
    FILTR    
    <div class="mr-auto"></div>
    <img src="./img/btn_right_filter.svg" alt="" width="40px" height="30px"
      :class="[ showLeftSidePanel ? 'rotate-arrow-180' : 'rotate-arrow-180-again' ]"
      style="cursor: pointer;">
  </div>

  <div 
    style="color: black;
    position: absolute;
    left: 190px;
    top: 13px;
    font-weight: 1000;
    font-size: 14px;
    cursor: pointer;"
    @click="showLeftSidePanel = !showLeftSidePanel">
    <img src="./img/badge.svg" alt="" width="25px" height="25px" 
      style="cursor: pointer;">
    <div class="centered">
      {{ sumFilter() }}
    </div>
  </div>

  <div 
    class="d-flex"
    :class="[ showLeftSidePanel ? 'filter-content-show' : 'filter-content-hide' ]"
    style="font-size: 18px; 
    width: 100%;"
    v-for="(row, index) in filter" 
    :key="row.id">
    <label 
      :class="[ !row.checked ? 'text-color-active' : 'text-color-default' ]"
      style="cursor: pointer;"      
      @click="filterPreventNull(row.id)">
      # {{ row.category }}
    </label>
    <div class="mr-auto"></div>
    <div 
      :class="[ row.checked ? 'checkbox-checked' : 'checkbox-unchecked' ]"
      style="cursor: pointer;"
      @click="filterPreventNull(row.id)">
    </div>
  </div>  

</div>