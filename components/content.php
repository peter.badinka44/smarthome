<div 
  class="content pl-4 pb-4 mt-2"
  :class="[ showLeftSidePanel ? 'product-filter-animation-out' : 'product-filter-animation-in' ]"
  :style="{ width: contentWidth() + 'px', height: 520 + 'px' }"
  style="font-family: 'Nunito';"
  ref="content">

  <div
    class="product text-white m-2 p-4"
    :style="{ backgroundImage: 'url(' + product.img + ')', backgroundSize: '350px 190px', backgroundPosition: 'left' }"
    style="font-weight: 800; 
    background-color: rgba(0, 0, 0, 0.8);"   
    v-for="product in products"
    :key="product.id"
    v-if="checkFilter(product)">

    <div style="font-size: 18px;">
      {{ product.head }}
    </div>   

    <div class="font-weight-normal"
      style="font-size: 16px;
      margin-top: 8px;
      margin-bottom: 12px;">
      {{ product.body }}
    </div>

    <button class="btn-zistit-vice">
      ZISTIT VICE
      <img src="./img/btn_arrow.svg" alt="" width="10px" height="10px" 
        style="margin-left: 5px; margin-bottom: 2px;">
    </button>

    <button 
      class="btn-chci-to ml-3"
      :class="[ checkBag(product) ? 'btn-bg-color-active' : 'btn-bg-color-default' ]"
      @click="addToBag(product)">
      CHCI TO
      <img src="./img/btn_check.svg" alt="" width="10px" height="10px"
        style="margin-left: 5px; margin-bottom: 2px;">
    </button>
  </div>

</div>