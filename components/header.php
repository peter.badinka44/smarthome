<div class="header">

  <img src='./img/logo_smartyhome.svg' alt="" width="100" height="100"  
  :class="[ showLeftSidePanel ? 'product-filter-animation-out' : 'product-filter-animation-in' ]"
  style="position: fixed;
  top: 20px;
  left: 80px;">

  <div class="d-flex weight-black"
    style="color: white;    
    position: fixed;
    top: 60px;
    left: 42%;
    font-size: 19.5px;
    font-weight: 900;">
    <div style="margin-right: 12px;">
      CHYTREJŠÍ
    </div>

    <div 
      class="header-animation-1"
      style="display: none;"
      id="headerAnimation1">
      DOMOV
    </div>

    <div 
      class="header-animation-2"
      style="display: none;"
      id="headerAnimation2">
      BYDLENÍ
    </div>

    <div 
      class="header-animation-3"
      style="display: none;"
      id="headerAnimation3">
      ŽIVOT
    </div>

    <div 
      class="header-animation-4"
      style="display: none;"
      id="headerAnimation4">
      FIRMA
    </div>

  </div>

</div>