<div class="footer"
  style="position: fixed;
  bottom: 20px;
  left: 45%;
  font-size: 14px;
  font-weight: 600;
  color: white;">
  <table style="text-align: center;">
    <tr>
      <td>
        Powered by ADT
      </td>
    </tr>
    <tr>
      <td>
      <img ref="footerLogo" src='./img/logo_ADT_white.svg' alt="" width="33" height="33" 
        class="mt-1"
        style="cursor: pointer;"
        @mouseover="footerLogoHover()"
        @mouseleave="footerLogoHoverOff()"
        @click="openFooterLink()">
      </td>
    </tr>
  </table>
</div>