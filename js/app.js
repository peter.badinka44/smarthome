(function () {	
	scrollConverter.activate();
}());

new Vue({
  el: '#app',
  data(){
    return {

      showLeftSidePanel: false,
      showRightSidePanel: false,
      footerHover: false,

      //-----------------------------------------------------------------
      products: [
        {
          id: 0,
          category: 'osvětlení',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 1,
          category: 'zabezpečení',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 2,
          category: 'senzory',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 3,
          category: 'informace',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 4,
          category: 'zdraví',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 5,
          category: 'čas',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 6,
          category: 'prezentace',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 7,
          category: 'osvětlení',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 8,
          category: 'zabezpečení',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 9,
          category: 'senzory',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 10,
          category: 'informace',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 11,
          category: 'zdraví',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 12,
          category: 'čas',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
        {
          id: 13,
          category: 'prezentace',
          head: 'LaMetric Time',
          body: 'Nejen chytré hodiny, počasí, notifikace z mobilného zařízení, cena Bitcoinu, počet foloweru...',
          img: "./img/product_1@2x.png"
        },
      ],

      //-----------------------------------------------------------------
      bag: [

      ],

      //-----------------------------------------------------------------
      filter: [
        {
          id: 0,
          category: 'osvětlení',
          checked: true,
        },
        {
          id: 1,
          category: 'zabezpečení',
          checked: true,
        },
        {
          id: 2,
          category: 'senzory',
          checked: true,
        },
        {
          id: 3,
          category: 'informace',
          checked: true,
        },
        {
          id: 4,
          category: 'zdraví',
          checked: true,
        },
        {
          id: 5,
          category: 'čas',
          checked: true,
        },       
      ]

    }
  },

  methods: {

    //-----------------------------------------------------------------
    addToBag(product) {
      if(!this.checkBag(product)) {
        this.bag.push(product)
      } else {
        this.removeFromBag(product)
      }
    },

    //-----------------------------------------------------------------
    removeFromBag(product) {
      this.bag = this.bag.filter(row => row.id != product.id)
    },

    //-----------------------------------------------------------------
    checkBag(product) {
      let result = false;
      this.bag.forEach(row => {        
        if(row.id == product.id) {
          result = true;
        }
      })
      return result;
    },

    //-----------------------------------------------------------------
    checkFilter(product) {
      let result = false;
      this.filter.forEach(row => {
        if(row.category == product.category && row.checked == true) {
          result = true;
        }
      })
      return result;
    },

    //-----------------------------------------------------------------
    sumFilter() {
      let result = 0;
      this.filter.forEach(row => {
        if(row.checked == true) {
          result++;
        }
      })      
      return result;
    },

    //-----------------------------------------------------------------
    sumFilterProducts() {
      let result = 0;
      this.filter.forEach(filter => {
        this.products.forEach(product => {
          if(filter.checked == true && product.category == filter.category) {
            result++
          }
        })
      })
      return result;
    },

    //-----------------------------------------------------------------
    filterPreventNull(id) {
      this.filter[id].checked = !this.filter[id].checked
      setTimeout(() => {
        if(this.sumFilter() == 0) {
          this.filter[id].checked = true
        }
      }, 50);      
    },

    //-----------------------------------------------------------------
    openFooterLink() {
      let win = window.open('https://www.appsdevteam.com/', '_blank')
			win.focus()
    },

    //-----------------------------------------------------------------
    footerLogoHover() {      
      this.$refs.footerLogo.src = './img/logo_ADT_blue.svg'
      this.footerHover = true
    },

    //-----------------------------------------------------------------
    footerLogoHoverOff() {      
      this.$refs.footerLogo.src = './img/logo_ADT_white.svg'
      this.footerHover = false
    },

    //-----------------------------------------------------------------
    contentWidth() {
      let sumProducts = this.sumFilterProducts()
      if(sumProducts % 2 == 1) sumProducts += 1
      let result = (405 * sumProducts) / 2
      return result;
    },

    //-----------------------------------------------------------------
    startHeaderAnimation() {
      animationIndex = 1
      document.getElementById('headerAnimation'+animationIndex).style.display = 'block';
      window.setInterval(function() {
        document.getElementById('headerAnimation'+animationIndex).style.display = 'none';
        animationIndex++;
        if(animationIndex > 4) animationIndex = 1;
        document.getElementById('headerAnimation'+animationIndex).style.display = 'block';
      }, 3000);
    },

    //-----------------------------------------------------------------
    toggleRightSidePanel() {
      document.getElementById('rightSidePanel').style.display = 'block';
      this.showRightSidePanel = !this.showRightSidePanel

    },

    //-----------------------------------------------------------------
    toggleLeftSidePanel() {
      document.getElementById('leftSidePanel').style.display = 'block';
      this.showLeftSidePanel = !this.showLeftSidePanel

    },

    //-----------------------------------------------------------------
    showBlueBag() {
      if(this.showRightSidePanel == true || this.bag.length > 0) return true;
      return false;
    },

    //-----------------------------------------------------------------
    showWhiteBag() {
      if(this.showRightSidePanel == false && this.bag.length == 0) return true;
      return false;
    }

  },

  mounted() {
    this.startHeaderAnimation()
  },

})