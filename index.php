<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" href="./img/logo_ADT_blue.svg"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito">
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/main.css">
  <title>Smart Home</title>
</head>
<body>

  <div 
    id="app" 
    class="grid-container">
    <?php include './components/header.php' ?>
    <?php include './components/content.php' ?>
    <?php include './components/left-side-panel.php' ?>
    <?php include './components/right-side-panel.php' ?>
    <?php include './components/footer.php' ?>
  </div>

  <script src="./js/vue-dev.js"></script>
  <script src="./js/vue-cookies.js"></script>
  <script src="./js/axios.min.js"></script>
  <script src="./js/jquery-3.5.1.min.js"></script>
  <script src="./js/popper.min.js"></script>
  <script src="./js/bootstrap.min.js"></script>
  <script src="./js/scroll-converter.min.js"></script>
  <script src="./js/app.js"></script>

</body>
</html>